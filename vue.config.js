'use strict'
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

// All configuration item explanations can be find in https://cli.vuejs.org/config/
// module.exports = {
//   configureWebpack: {
//     resolve: {
//       alias: {
//         '@': resolve('src')
//       }
//     },
//     devtool: 'source-map'
//   }
// }
module.exports = {
    productionSourceMap: false,
    configureWebpack: {
      module: {
        rules: [
          {
            test: /\.(csv|xlsx|xls)$/,
            loader: 'file-loader',
            options: {
              name: `files/[name].[ext]`
            }
          }
        ],
       },
  }
}

