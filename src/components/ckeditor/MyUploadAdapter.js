import axios from "axios"
class MyUploadAdapter {
    constructor(loader) {
        this.loader = loader;
    }

    async upload() {
        let respone = ''
        const data = new FormData();
        data.append('file', await this.loader.file);
        let filePath = this.loader.file
        if (this.loader.file) {
            // console.log('size: ' + this.loader._reader.total)
            // if (this.loader._reader.total > 10497) {
            //     alert('Size ảnh quá lớn, chọn size ảnh nhỏ hơn')
            // } else {
                await axios({
                    url: 'https://localhost:44360/Upload',
                    method: 'post',
                    data,
                    headers: {
                        'Content-Type': 'multipart/form-data;'
                    }
                }).then(res => {
                    if (res) {
                        respone = res.data
                    }
                })
            // }
            return {
                default: respone
            };
        }

    }

}

export default MyUploadAdapter;