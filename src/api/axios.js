import axios from "axios";

axios.defaults.baseURL = 'https://flearnapi.azurewebsites.net/';
// axios.defaults.baseURL = 'https://localhost:44360/';
axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem("token");