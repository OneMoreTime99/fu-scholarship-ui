import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './api/axios'


Vue.config.productionTip = false

import "../public/css/materialdesignicons.min.css";
import 'swiper/swiper-bundle.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { BootstrapVue } from 'bootstrap-vue'
var VueScrollTo = require('vue-scrollto');
import Scrollspy from 'vue2-scrollspy';
import VueYoutube from 'vue-youtube';
import vueVimeoPlayer from 'vue-vimeo-player';
import VueMasonry from 'vue-masonry-css';
import VueSweetalert2 from 'vue-sweetalert2';
import Vuelidate from 'vuelidate';
import moment from 'moment';
import VueResource from "vue-resource"
import CKEditor from '@ckeditor/ckeditor5-vue2';
import VueFbCustomerChat from 'vue-fb-customer-chat';


import 'sweetalert2/dist/sweetalert2.min.css';


Vue.use(VueScrollTo, {
  duration: 3000,
  easing: "ease"
})
Vue.filter('formatDate', function(value) {
  if (value) {
      return moment(String(value)).format('DD/MM/YYYY')
  }
});
Vue.use(Vuelidate);
Vue.use(BootstrapVue);
Vue.use(Scrollspy);
Vue.use(VueYoutube);
Vue.use(vueVimeoPlayer);
Vue.use(VueMasonry);
Vue.use(VueSweetalert2);
Vue.use(CKEditor);
Vue.use(VueResource);
Vue.use(VueFbCustomerChat, {
  page_id: 104526561960172, //  change 'null' to your Facebook Page ID,
  theme_color: '#3498DB', // theme color in HEX
  locale: 'vi_VN', // default 'en_US'
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
