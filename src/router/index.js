import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    //HV
    path: "/",
    name: "Home",
    component: () => import("../views/index-course"),
  },
  {
    //HV
    path: "/contact-us",
    name: "Contact",
    component: () => import("../views/contact-us"),
  },
  {
    //HV
    path: "/about-us",
    name: "AboutUs",
    component: () => import("../views/about-us"),
  },
  {
    path: "/manager-course",
    name: "CourseManager",
    component: () => import("../views/manager-course")
  },
  {
    path: "/mentor-course-detail",
    name: "MentorCourseDetail",
    component: () => import("../views/mentor-course-detail"),
  },
  {
    //HV
    path: "/auth-login",
    name: "Login",
    component: () => import("../views/auth-login"),
  },
  {
    //HV
    path: "/auth-signup",
    name: "Signup",
    component: () => import("../views/auth-signup"),
  },
  {
    //HV
    path: "/auth-re-password",
    name: "Repassword",
    component: () => import("../views/auth-re-password"),
  },
  {
    path: "/create-course",
    name: "CreateCourse",
    component: () => import("../views/create-course"),
  },
  {
    //HV
    path: "/navbar",
    name: "Navbar",
    component: () => import("../components/navbarnew"),
  },
  {
    //HV
    path: "/profile",
    name: "Profile",
    component: () => import("../views/account-profile"),
  },
  {
    path: "/learner-main-page",
    name: "LearnerMainPage",
    component: () => import("../views/learner-main-page"),
  },
  {
    path: "/modify-course",
    name: "ModifyCourse",
    component: () => import("../views/modify-course"),
  },
  {
    //HV
    path: "/manager-account",
    name: "ManagerAccount",
    component: () => import("../views/manager-account"),
  },
  {
    path: "/learner-home",
    name: "LearnerHome",
    component: () => import("../views/learner-home"),
  },
  {
    path: "/learner-list-course",
    name: "LearnerListCourse",
    component: () => import("../views/learner-list-course"),
  },
  {
    path: '/learner-lesson-detail',
    name: 'LearnerLessonDetail',
    component: () => import('../views/learner-lesson-detail')
  },
  {
    path: "/learner-list-exam",
    name: "LearnerListExam",
    component: () => import("../views/learner-list-exam"),
  },
  {
    path: "/learner-exam-detail",
    name: "LearnerExamDetail",
    component: () => import("../views/learner-exam-detail"),
  },
  {
    path: "/learner-quiz",
    name: "LearnerDoQuiz",
    component: () => import("../views/learner-quiz"),
  },
  {
    path: "/manager-home",
    name: "ManagerHome",
    component: () => import("../views/manager-home"),
  },
  {
    path: "/manager-list-group-question",
    name: "ManagerListGroupQuestion",
    component: () => import("../views/manager-list-group-question"),
  },
  {
    path: "/manager-create-group-question",
    name: "CreateGroupQuestion",
    component: () => import("../views/manager-create-group-question"),
  },
  {
    path: "/manager-edit-group-question",
    name: "EditGroupQuestion",
    component: () => import("../views/manager-edit-group-question"),
  },
  {
    path: "/manager-group-question-detail",
    name: "DetailGroupQuestion",
    component: () => import("../views/manager-group-question-detail"),
  },
  {
    //HV
    path: "/create-account",
    name: "CreateAccount",
    component: () => import("../views/create-account"),
  },
  // {
  //   //HV
  //   path: "/view-account",
  //   name: "ViewAccount",
  //   component: () => import("../views/view-account"),
  // },
   {
    //tuan
    path: "/mentor-account",
    name: "MentorAccount",
    component: () => import("../views/mentor-account"),
  },
  {
    //tuan
    path: "/learner-account",
    name: "LearnerAccount",
    component: () => import("../views/learner-account"),
  },
  {
    //tuan
    path: "/managerLst-account",
    name: "ManagetLstAccount",
    component: () => import("../views//managerLst-account"),
  },
  {
    //HV
    path: "/edit-account",
    name: "EditAccount",
    component: () => import("../views/edit-account"),
  },
  //T.Kiên
  {
    path: "/learner-course-detail",
    name: "LearnerCourseDetail",
    component: () => import("../views/learner-course-detail"),
  },
  {
    path: "/view-profile",
    name: "ViewProfile",
    component: () => import("../views/view-profile"),
    props: true,
  },
  {
    //tuan code
    path: "/create-group",
    name: "CreateGroup",
    component: () => import("../views/create-group"),
  },
  {
    //tuan code
    path: "/modify-group",
    name: "ModifyGroup",
    component: () => import("../views/modify-group"),
  },
  {
    //tuan code
    path: "/manager-group",
    name: "ManagerGroup",
    component: () => import("../views/manager-group"),
  },
  {
    //tuan code
    path: "/group-detail",
    name: "DetailGroup",
    component: () => import("../views/group-detail"),
  },
  {
    path: '/ckeditor',
    name: 'CKEditor',
    component: () => import('../components/ckeditor/ckeditor')
  },
  {
    path: '/upload-image',
    name: 'UploadImage',
    component: () => import('../components/UploadImage')
  },
  {
    path: "/edit-account-by-user",
    name: "EditAccountByUser",
    component: () => import("../views/edit-account-by-user"),
  },
  {
    path: "/manager-question",
    name: "QuestionManager",
    component: () => import("../views/manager-question"),
  },
  {
    path: "/mentor-list-lesson",
    name: "MentorListLesson",
    component: () => import("../views/mentor-list-lesson")
  },
  {
    path: "/create-question",
    name: "CreateQuestion",
    component: () => import("../views/create-question"),
  },
  {
    path: "/question-detail",
    name: "QuestionDetail",
    component: () => import("../views/question-detail"),
  },
  {
    path: "/edit-question",
    name: "EditQuestion",
    component: () => import("../views/edit-question"),
  },
  {
    path: "/create-quiz",
    name: "CreateQuiz",
    component: () => import("../views/create-quiz"),
  },
  {
    path: "/edit-quiz",
    name: "EditQuiz",
    component: () => import("../views/edit-quiz"),
  },
  {
    path: "/mentor-quiz-detail",
    name: "MetorQuizDetail",
    component: () => import("../views/mentor-quiz-detail"),
  },
  {
    path: "/mentor-lesson-detail",
    name: "MentorLessonDetail",
    component: () => import("../views/mentor-lesson-detail"),
  },
  {
    path: "/mentor-modify-lesson",
    name: "MentorModifyLesson",
    component: () => import("../views/mentor-modify-lesson"),
  },
  {
    path: "/mentor-create-lesson",
    name: "MentorCreateLesson",
    component: () => import("../views/mentor-create-lesson")
  },
  {
    //
    path: "/select-question",
    name: "SelectQuestionInGroup",
    component: () => import("../views/select-question-ingroup-dialog"),
  },
  {
    path: "/manager-matrix",
    name: "ManagerMatrix",
    component: () => import("../views/manager-matrix"),
  },
  {
    path: "/create-matrix",
    name: "CreateMatrix",
    component: () => import("../views/create-matrix"),
  },
  {
    path: "/matrix-detail",
    name: "MatrixDetail",
    component: () => import("../views/matrix-detail"),
  },
  {
    path: "/edit-matrix",
    name: "EditMatrix",
    component: () => import("../views/edit-matrix"),
  },
  {
    path: "/manager-exam-list",
    name: "ManagerExamList",
    component: () => import("../views/manager-exam-list"),
  },
  {
    path: "/create-exam",
    name: "CreateExam",
    component: () => import("../views/create-exam"),
  },
  {
    path: "/edit-exam",
    name: "EditExam",
    component: () => import("../views/edit-exam"),
  },
  {
    path: "/manager-exam-detail",
    name: "ManagerExamDetail",
    component: () => import("../views/manager-exam-detail"),
  },
  {
    path: "/exam-question",
    name: "ExamQuestion",
    component: () => import("../views/exam-question"),
  },
  {
    path: "/exam-test",
    name: "ExamTest",
    component: () => import("../views/exam-test"),
  },
  {
    path: "/manager-test-detail",
    name: "ManagerTestDetail",
    component: () => import("../views/manager-test-detail"),
  },
  {
    path: "/exam-learner",
    name: "ExamLearner",
    component: () => import("../views/exam-learner"),
  },
  {
    path: "/exam-learner-detail",
    name: "ExamLearnerDetail",
    component: () => import("../views/exam-learner-detail"),
  },
  {
    path: "/learner-view-quiz",
    name: "LearnerViewQuiz",
    component: () => import("../views/learner-view-quiz"),
  },
  {
    path: "/exam-score-list",
    name: "ExamScoreList",
    component: () => import("../views/exam-score-list"),
  },
  {
    path: "/learner-exam",
    name: "LearnerExam",
    component: () => import("../views/learner-exam"),
  },
  {
    path: "/import-question",
    name: "ImportQuestion",
    component: () => import("../views/import-question"),
  },
  {
    path: "/rank",
    name: "Rank",
    component: () => import("../views/rank"),
  },
  {
    path: "/rank-exam-list",
    name: "RankExamList",
    component: () => import("../views/rank-exam-list"),
  },
  { 
    path: '/edit-account-admin', 
    name: 'EditAccountAdmin',
    component: () => import("../views/edit-account-by-admin") 
  },
  { 
    path: '/:pathMatch(.*)*', 
    component: () => import("../views/page-error") 
  }
  ,
  { 
    path: '/:pathMatch(.*)/*', 
    name: 'Notfound',
    component: () => import("../views/page-error") 
  }
];


const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});


export default router;
