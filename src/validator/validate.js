import { helpers as vuelidateHelpers } from 'vuelidate/lib/validators'

export const validateHasSpecialCharacter1 = value => {
  if (!vuelidateHelpers.req(value)) {
    return true
  }
  const match = value.match(/^[a-zA-Z0-9_@#$%^&*()~<>?{}\\|]{3,16}$/g) || []
  return match.length >= 1
}

export function validateHasSpecialCharacter(value) {
  return !!(/^[a-zA-Z0-9_@#$%^&*()~<>?{}\\|]{3,16}$/.test(value))
}

export function validatePassword(value) {
  return !!(/^[a-zA-Z0-9_@#$%^&*()~<>?{}\\|]{8,100}$/.test(value))
}

export function validateEmail(value) {
  return !!(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value))
}

export function validatePhone(value) {
  return !!(/^(84|0[3|5|7|8|9])+([0-9]{8})\b$/.test(value))
}

export function validateNumberGreaterThan0(value) {
  return !!(/^[1-9]\d*$/.test(value))
}